import { PrzychodniaClientPage } from './app.po';

describe('przychodnia-client App', function() {
  let page: PrzychodniaClientPage;

  beforeEach(() => {
    page = new PrzychodniaClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
